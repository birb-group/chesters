<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m210327_153419_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function Up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique()->notNull(),
            'email' => $this->string()->unique()->notNull(),
            'password' => $this->string()->notNull(),
            'register_token' => $this->string()->notNull(),
            'register_timestamp' => $this->string()->notNull(),
            'role' => $this->integer()->defaultValue(1),
            'status' => $this->integer()->defaultValue(0),
        ]);

        $this->insert('user', [
            'username' => 'chesters',
            'email' => 'chesters@mail.ru',
            'register_token' => Yii::$app->security->generateRandomString(),
            'password' => md5('chesters_2021'),
            'register_timestamp' => date('d.m.Y H:i:s'),
            'role' => 2,
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        $this->dropTable('{{%user}}');
    }
}
