<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m210328_044502_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique()->notNull(),
            'category_id' => $this->integer()->defaultValue(1), // id категории, к которой относится товар
            'vendor_code' => $this->integer()->unique()->notNull(), // артикул товара
            'description' => $this->text()->null(),
            'cost' => $this->decimal(8, 2)
        ]);

        $this->createIndex('category_idx', 'product', 'category_id');
        $this->addForeignKey('category_fk', 'product', 'category_id', 'category', 'id');

        $this->insert('product', [
            'name' => 'Тестовый товар 1',
            'vendor_code' => 232342,
            'cost' => 10.2
        ]);

        $this->insert('product', [
            'name' => 'Тестовый товар 2',
            'vendor_code' => 232342,
            'cost' => 10.2
        ]);

        $this->insert('product', [
            'name' => 'Тестовый товар 3',
            'vendor_code' => 232342,
            'cost' => 10.2
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('category_fk', 'product');
        $this->dropIndex('category_idx', 'product');

        $this->dropTable('{{%product}}');
    }
}
