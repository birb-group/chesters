<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category}}`.
 */
class m210328_044453_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'parent_category_id' => $this->integer()->null(),
        ]);

        $this->createIndex('parent_category_idx', 'category', 'parent_category_id');
        $this->addForeignKey('parent_category_fk', 'category', 'parent_category_id', 'category', 'id');

        $this->insert('category', [
            'id' => 1,
            'name' => 'Разное'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('parent_category_fk', 'category');
        $this->dropIndex('parent_category_idx', 'category');

        $this->dropTable('{{%category}}');
    }
}
