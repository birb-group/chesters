function addToBasket(id) {
    let count = $('#' + id).find('.count');
    let newCount = parseInt(count.text()) + 1;
    count.text(newCount);

    $.post('/basket/add', {"id": id});
}

function removeFromBasket(id) {
    $('#' + id).remove();

    if (!$('.product-list-item').length) {
        clearBasket();
    }
    $.post('/basket/dell', {"id": id});
}

function removeOneFromBasket(id) {
    let count = $('#' + id).find('.count');
    let newCount = parseInt(count.text()) - 1;
    if (parseInt(count.text()) != 0) {
        count.text(newCount);
    } else {
        removeFromBasket(id);
    }

    $.post('/basket/dell-one', {"id": id});
}

function clearBasket() {
    $.post('/basket/clear');
}