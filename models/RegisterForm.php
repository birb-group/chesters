<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $access;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password', 'password_repeat', 'email', 'access'], 'required', 'message' => 'Поле {attribute} не должно быть пустым.'],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password'],
            [['access'], 'boolean'],
            [['access'], 'compare', 'compareValue' => true],
            [['username'], 'match', 'pattern' => '/^[a-zA-Z]\w*$/i'],
            [['username'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'username'],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'password_repeat' => 'Повтор пароля',
            'email' => 'Email',
            'access' => 'Согласие на обработку персональных данных',
        ];
    }

    public function contact($email, $registration_token)
    {
        Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setReplyTo([$this->email => $this->username])
            ->setSubject("Подтверждение регистрации")
            ->setTextBody("http://chesters/site/registration-accept?registration_token=$registration_token")
            ->send();
    }
}
