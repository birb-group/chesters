<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $register_token
 * @property string $register_timestamp
 * @property int|null $role
 * @property int|null $status
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password'], 'required'],
            [['role', 'status'], 'integer'],
            [['username', 'email', 'password', 'register_token', 'register_timestamp'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'register_token' => 'Register Token',
            'register_timestamp' => 'Register Timestamp',
            'role' => 'Role',
            'status' => 'Status',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return null;
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        return $this->password == md5($password);
    }

    public function validateStatus($username)
    {
        return self::findOne(['username' => $username])->status == 1;
    }

    public function isAdmin()
    {
        return $this->role == 2;
    }

    public function beforeSave($insert)
    {
        $this->password = md5($this->password);
        $this->register_timestamp = date('d.m.Y H:i:s');
        $this->register_token = Yii::$app->security->generateRandomString();

        return parent::beforeSave($insert);
    }
}
