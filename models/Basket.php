<?php

namespace app\models;

use yii\base\Model;
use Yii;

class Basket extends Model
{
    /**
     * Метод добавляет товар в корзину
     */
    public function addToBasket($id)
    {
        $product = Product::findOne($id);
        $session = Yii::$app->session;
        $session->open();
        if (!$session->has('basket')) {
            $session->set('basket', []);
            $basket = [];
        } else {
            $basket = $session->get('basket');
        }
        if (isset($basket['products'][$product->id])) { // такой товар уже есть?
            $basket['products'][$product->id]['count']++;
        } else { // такого товара еще нет
            $basket['products'][$product->id]['name'] = $product->name;
            $basket['products'][$product->id]['cost'] = $product->cost;
            $basket['products'][$product->id]['count'] = 1;
        }
        $session->set('basket', $basket);
    }

    /**
     * Метод удаляет товар из корзины
     */
    public function removeFromBasket($id)
    {
        $id = abs((int)$id);
        $session = Yii::$app->session;
        $session->open();
        if (!$session->has('basket')) {
            return;
        }
        $basket = $session->get('basket');
        if (!isset($basket['products'][$id])) {
            return;
        }
        unset($basket['products'][$id]);
        if (count($basket['products']) == 0) {
            $session->set('basket', []);
            return;
        }
        $amount = 0.0;
        foreach ($basket['products'] as $item) {
            $amount = $amount + $item['cost'] * $item['count'];
        }
        $basket['amount'] = $amount;

        $session->set('basket', $basket);
    }

    /**
     * Метод удаляет один товар из корзины
     */
    public function removeOneFromBasket($id)
    {
        $id = abs((int)$id);
        $session = Yii::$app->session;
        $session->open();
        if (!$session->has('basket')) {
            return;
        }
        $basket = $session->get('basket');
        if (!isset($basket['products'][$id])) {
            return;
        }
        if ($basket['products'][$id]['count'] == 1) {
            unset($basket['products'][$id]);
        } else {
            $basket['products'][$id]['count']--;
        }
        if (count($basket['products']) == 0) {
            $session->set('basket', []);
            return;
        }
        $amount = 0.0;
        foreach ($basket['products'] as $item) {
            $amount = $amount + $item['cost'] * $item['count'];
        }
        $basket['amount'] = $amount;

        $session->set('basket', $basket);
    }

    /**
     * Метод возвращает содержимое корзины
     */
    public function getBasket()
    {
        $session = Yii::$app->session;
        $session->open();
        if (!$session->has('basket')) {
            $session->set('basket', []);
            return [];
        } else {
            return $session->get('basket');
        }
    }

    /**
     * Метод удаляет все товары из корзины
     */
    public function clearBasket()
    {
        $session = Yii::$app->session;
        $session->open();
        $session->set('basket', []);
    }
}