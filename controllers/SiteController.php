<?php

namespace app\controllers;

use app\models\Product;
use app\models\RegisterForm;
use app\models\User;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'add-to-cart'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegistration()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $user = new User();
            if ($user->load(['username' => $model->username, 'email' => $model->email, 'password' => $model->password], '') && $user->save()) {
                Yii::$app->session->setFlash('success',
                    'Вы успешно зарегистрированы, для завершения регистрации
                    перейдите по ссылке из сообщения,
                    которое было отправлено вам на указанную электронную почту.');

                $model->contact($model->email, $user->register_token);

                return $this->redirect(['/']);
            }
        }

        $model->password = '';
        $model->password_repeat = '';

        return $this->render('registration', [
            'model' => $model,
        ]);
    }

    public function actionRegistrationAccept()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goBack();
        }

        $register_token = Yii::$app->request->get('registration_token');

        if ($register_token) {
            $user = User::findOne(['register_token' => $register_token]);
            if ($user->status == 1) {
                Yii::$app->session->setFlash('success', 'Аккаунт ' . $user->username . ' уже активирован!');
                return $this->goBack();
            }

            if ($user->updateAttributes(['status' => 1])) {
                Yii::$app->session->setFlash('success', 'Аккаунт ' . $user->username . ' успешно активирован!');
                return $this->redirect(['login']);
            }
        }

        return $this->goBack();
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
