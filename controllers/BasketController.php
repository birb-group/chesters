<?php

namespace app\controllers;

use app\models\Basket;
use app\models\Product;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

class BasketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['index', 'add', 'dell', 'dell-one', 'clear'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $basket = (new Basket())->getBasket();
        if (!$basket) {
            return Yii::$app->response->redirect('/site/index');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->where(['id' => array_keys($basket['products'])])
        ]);
        return $this->render('index', ['basket' => $basket, 'dataProvider' => $dataProvider]);
    }

    public function actionAdd()
    {
        $basket = new Basket();
        $data = Yii::$app->request->post();
        $basket->addToBasket($data['id']);
    }

    public function actionDell()
    {
        $basket = new Basket();
        $data = Yii::$app->request->post();

        $basket->removeFromBasket($data['id']);
    }

    public function actionDellOne()
    {
        $basket = new Basket();
        $data = Yii::$app->request->post();

        $basket->removeOneFromBasket($data['id']);
    }

    public function actionClear()
    {
        $basket = new Basket();
        $basket->clearBasket();
        return Yii::$app->response->redirect('/product');
    }
}