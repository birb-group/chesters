<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<section>
    <div class="products-list">
        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list',
            'viewParams' => ['basket' => $basket],
            'summary' => false,
            'itemOptions' => [
                'tag' => false,
            ],
        ]) ?>
    </div>
    <div class="row">
        <?= Html::button('Отчистить корзину', ['class' => 'btn btn-danger col-md-2', 'onclick' => 'clearBasket()']) ?>
        <?php //echo Html::a('Оформить заказ', '/order', ['class' => 'btn btn-success col-md-2',]) ?>
    </div>
</section>

