<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model app\models\Product */
?>

<div id="<?= $model->id ?>" class="product-list-item">
    <div class="d-flex">
        <span class="text-uppercase"><?= $model->name ?></span>
        <?= Html::button('+', ['class' => 'btn btn-success', 'onclick' => 'addToBasket(' . $model->id . ')']) ?>

        <span class="count text-uppercase"><?= $basket['products'][$model->id]['count'] ?></span>

        <?= Html::button('-', ['class' => 'btn btn-danger', 'onclick' => 'removeOneFromBasket(' . $model->id . ')']) ?>

        <?= Html::button('Удалить из корзины', ['class' => 'btn btn-danger', 'onclick' => 'removeFromBasket(' . $model->id . ')']) ?>
    </div>
    <h2><?= $model->cost; ?> руб.</h2>
</div>