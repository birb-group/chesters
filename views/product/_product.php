<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

?>
<div class="product-card">
    <div>
        <div class="col-md-5">
            <h1><?= Html::a($model->name, ['/product/view', 'id' => $model->id]) ?></h1>
        </div>
        <div class="col-md-6">
            <h2><?= $model->cost; ?> руб.</h2>
            <?= Html::button('Добавить в корзину', ['class' => 'btn btn-warning', 'onclick' => 'addToBasket(' . $model->id . ')']) ?>
        </div>
    </div>
</div>
