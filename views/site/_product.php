<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */

?>
<div class="product-card">
    <?= print_r($cart) ?>
    <p>Название: <?= $cart['name'] ?></p>

    <p>Цена: <?= $cart['cost'] ?></p>

    <p>
        <a class="btn btn-primary"
           href="/product/view?id=<?= $model->id ?>">Подробнее
        </a>
        <button class="add-to-cart btn btn-success"
                data-product="<?= $model->id ?>">В корзину
        </button>
    </p>
</div>
